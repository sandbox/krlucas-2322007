<?php

/**
 * @file
 * Panels Manual pipeline declaration.
 */

/**
 * Implements hook_default_panels_renderer_pipeline().
 */
function panels_np_default_panels_renderer_pipeline() {
  $pipelines = array();

  $pipeline = new stdClass();
  $pipeline->disabled = FALSE; /* Edit this to true to make a default pipeline disabled initially */
  $pipeline->api_version = 1;
  $pipeline->name = 'panels_np_renderer';
  $pipeline->admin_title = t('Panels Named Panes');
  $pipeline->admin_description = t('Provides panel panes to the layout as standalone variables so they can be arranged manually.');
  $pipeline->weight = -100;
  $pipeline->settings = array(
    'renderers' => array(
      0 => array(
        'access' => array(),
        'renderer' => 'panels_np_renderer',
        'options' => array(),
      ),
    ),
  );
  $pipelines[$pipeline->name] = $pipeline;

  return $pipelines;
}
