<?php
/**
 * Renders panel panes directly to the layout ignoring regions and placement.
 */
class panels_np_renderer extends panels_renderer_standard {
  /**
   * {@inheritdoc}
   */
  function render_layout() {
    if (empty($this->prep_run)) {
      $this->prepare();
    }
    $this->render_panes();
    $this->rendered['named_panes'] = array();
    $this->rendered['named_panes_titles'] = array();
    foreach ($this->display->content as $key => $content) {
      if (!empty($content->configuration['variable_name'])) {
        $this->rendered['named_panes'][$content->configuration['variable_name']] = !empty($this->rendered['panes'][$key]) ? $this->rendered['panes'][$key] : '';
        $this->rendered['named_panes_titles'][$content->configuration['variable_name']] = '';
        if (!empty($content->configuration['title']) && empty($content->configuration['override_title'])) {
          $this->rendered['named_panes_titles'][$content->configuration['variable_name']] = filter_xss($content->configuration['title']);
        }
        if (!empty($content->configuration['override_title'])) {
          $this->rendered['named_panes_titles'][$content->configuration['variable_name']] = filter_xss($content->configuration['override_title_text']);
        }
      }
      else {
        $this->rendered['named_panes'][$content->type] = !empty($this->rendered['panes'][$key]) ? $this->rendered['panes'][$key] : '';
      }
    }

    if ($this->admin && !empty($this->plugins['layout']['admin theme'])) {
      $theme = $this->plugins['layout']['admin theme'];
    }
    else {
      $theme = $this->plugins['layout']['theme'];
    }

    $this->rendered['layout'] = theme($theme, array(
      'css_id' => check_plain($this->display->css_id),
      'content' => $this->rendered['named_panes'],
      'content_titles' => $this->rendered['named_panes_titles'],
      'settings' => $this->display->layout_settings,
      'display' => $this->display,
      'layout' => $this->plugins['layout'],
      'renderer' => $this,
    ));
    return $this->prefix . $this->rendered['layout'] . $this->suffix;
  }
}
